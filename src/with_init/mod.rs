use std::fmt;

pub trait Pipeline: fmt::Debug {
	type Input;
	type Output;

	fn init(&mut self, input: Self::Input) -> Self::Output;

	fn exec(&mut self, input: Self::Input) -> Self::Output;

	fn and<S>(self, next: S) -> PipelineAnd<Self, S>
	where
		S: Pipeline<Input = Self::Output>,
		Self: Sized,
	{
		PipelineAnd {
			this: self,
			next: next,
		}
	}
}

pub struct PipelineAnd<S, N>
where
	S: Pipeline,
	N: Pipeline<Input = S::Output>,
{
	this: S,
	next: N,
}

impl<S, N> fmt::Debug for PipelineAnd<S, N>
where
	S: Pipeline,
	N: Pipeline<Input = S::Output>,
{
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		self.next.fmt(f)
	}
}

impl<S, N> Pipeline for PipelineAnd<S, N>
where
	S: Pipeline,
	N: Pipeline<Input = S::Output>,
{
	type Input = S::Input;
	type Output = N::Output;

	fn init(&mut self, input: Self::Input) -> Self::Output {
		self.next.init(self.this.init(input))
	}

	fn exec(&mut self, input: Self::Input) -> Self::Output {
		self.next.exec(self.this.exec(input))
	}
}
